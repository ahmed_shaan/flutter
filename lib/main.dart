import "package:flutter/material.dart";
import 'package:flutterapp/app_screens/first_screen.dart';


void main() => runApp(new MyFlutterApp());

class MyFlutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "my Flutter app",
        home: Scaffold(
          appBar: AppBar(title: Text("Mudhalu Zakai"),),
          body:FirstScreen()
        )
    );
  }

}